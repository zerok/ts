.PHONY: all test clean

all: bin/ts

bin/ts: bin go.mod go.sum $(shell find . -name '*.go')
	cd cmd/ts && go build -o ../../$@

bin:
	mkdir -p bin

clean:
	rm -rf bin

test:
	go test -v ./...
