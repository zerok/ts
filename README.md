# ts: Timestamp utilities

This is a little commandline tool for parsing and reformatting
timestamps.

```
$ ts
1605212651

$ ts --out-format rfc3339
2020-11-12T21:24:34+01:00

$ ts --out-format rfc3339 1605210000
2020-11-12T20:40:00+01:00
```

The list of supported input and output formats will most likely evolve
over time. You can get a complete list using the `--in-formats` and
`--out-formats` flags.
