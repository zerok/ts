package main

import (
	"fmt"
	"os"
	"time"

	"github.com/spf13/pflag"
	"gitlab.com/zerok/ts/internal/formatter"
	"gitlab.com/zerok/ts/internal/parser"
)

var version string
var commit string
var date string

func main() {
	var inFormat string
	var outFormat string
	var listInFormats bool
	var listOutFormats bool
	var showVersion bool
	pflag.StringVar(&inFormat, "in-format", "unix", "Input format")
	pflag.StringVar(&outFormat, "out-format", "unix", "Output format")
	pflag.BoolVar(&listInFormats, "in-formats", false, "List all supported input formats")
	pflag.BoolVar(&listOutFormats, "out-formats", false, "List all supported input formats")
	pflag.BoolVar(&showVersion, "version", false, "Show version information")
	pflag.Parse()

	if showVersion {
		fmt.Printf("Version %s\nCommit: %s\nBuild date: %s\n", version, commit, date)
		os.Exit(0)
	}
	if listInFormats {
		for _, f := range parser.SupportedFormats() {
			fmt.Println(f)
		}
		os.Exit(0)
	}
	if listOutFormats {
		for _, f := range formatter.SupportedFormats() {
			fmt.Println(f)
		}
		os.Exit(0)
	}

	args := pflag.Args()
	t := time.Now()
	if len(args) > 0 {
		ts, err := parser.Parse(inFormat, args[0])
		if err != nil {
			panic(err)
		}
		t = ts
	}

	out, err := formatter.Format(outFormat, t)
	if err != nil {
		panic(err)
	}
	fmt.Println(out)
}
