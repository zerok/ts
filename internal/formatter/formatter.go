package formatter

import (
	"fmt"
	"math/big"
	"strings"
	"time"
)

// ErrUnknownformat is returned by Format if the specified format is
// unknown.
var ErrUnknownFormat = fmt.Errorf("unknown format")

type formatter func(time.Time, string) (string, error)

func formatRFC3339(t time.Time, options string) (string, error) {
	return t.Format(time.RFC3339), nil
}
func formatUnix(t time.Time, options string) (string, error) {
	return fmt.Sprintf("%d", t.Unix()), nil
}
func formatUnixNano(t time.Time, options string) (string, error) {
	return fmt.Sprintf("%d", t.UnixNano()), nil
}
func formatCustom(t time.Time, options string) (string, error) {
	return t.Format(options), nil
}
func formatMicrosoft(t time.Time, options string) (string, error) {
	ref := time.Date(1601, 1, 1, 0, 0, 0, 0, time.UTC)
	refOffset := big.NewInt(ref.Unix() * -1)
	final := big.NewInt(0)
	final = final.Add(refOffset, big.NewInt(t.Unix()))
	final.Mul(final, big.NewInt(10000000))
	return fmt.Sprintf("%v", final), nil
}

// Format returns the Time formatted using one of the predefined
// formats. If the format is invalid or not supported, an error is
// returned.
func Format(format string, t time.Time) (string, error) {
	formatters := map[string]formatter{
		"rfc3339":   formatRFC3339,
		"unix":      formatUnix,
		"unixnano":  formatUnixNano,
		"custom":    formatCustom,
		"microsoft": formatMicrosoft,
	}
	elems := strings.SplitN(format, ":", 2)
	fmt, ok := formatters[elems[0]]
	if !ok {
		return "", ErrUnknownFormat
	}
	if len(elems) > 1 && ok {
		return fmt(t, elems[1])
	}
	return fmt(t, "")
}

// SupportedFormats returns a list of all supported formats.
func SupportedFormats() []string {
	return []string{"rfc3339", "unix", "unixnano", "custom", "microsoft"}
}
