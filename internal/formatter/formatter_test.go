package formatter_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/zerok/ts/internal/formatter"
)

type testCase struct {
	input    time.Time
	expected string
}

func TestFormat(t *testing.T) {
	ts := time.Date(2020, 11, 12, 12, 0, 0, 0, time.UTC)
	tsUnix := 1605182400

	t.Run("unix", func(t *testing.T) {
		out, err := formatter.Format("unix", ts)
		require.NoError(t, err)
		require.Equal(t, fmt.Sprintf("%d", tsUnix), out)
	})
	t.Run("unixnano", func(t *testing.T) {
		out, err := formatter.Format("unixnano", ts)
		require.NoError(t, err)
		require.Equal(t, fmt.Sprintf("%d000000000", tsUnix), out)
	})
	t.Run("microsoft", func(t *testing.T) {
		for _, test := range []testCase{
			{
				input:    time.Date(2020, 11, 12, 12, 0, 0, 0, time.UTC),
				expected: "132496560000000000",
			},
			{
				input:    time.Date(1601, 1, 1, 0, 0, 0, 0, time.UTC),
				expected: "0",
			},
			{
				input:    time.Date(1599, 1, 1, 0, 0, 0, 0, time.UTC),
				expected: "-631584000000000",
			},
		} {
			out, err := formatter.Format("microsoft", test.input)
			require.NoError(t, err)
			require.Equal(t, test.expected, out)
		}
	})
}
