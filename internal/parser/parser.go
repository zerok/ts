package parser

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

var ErrUnknownFormat = fmt.Errorf("unknown format")

type parser func(raw string, options string) (time.Time, error)

func parseUnix(raw string, options string) (time.Time, error) {
	v, err := strconv.ParseInt(raw, 10, 64)
	if err != nil {
		return time.Time{}, err
	}
	return time.Unix(v, 0), nil
}

func Parse(format string, raw string) (time.Time, error) {
	parsers := map[string]parser{
		"unix": parseUnix,
	}
	elems := strings.SplitN(format, ":", 2)
	fmt, ok := parsers[elems[0]]
	if !ok {
		return time.Time{}, ErrUnknownFormat
	}
	if len(elems) > 1 && ok {
		return fmt(raw, elems[1])
	}
	return fmt(raw, "")
}

// SupportedFormats returns a list of all supported formats.
func SupportedFormats() []string {
	return []string{"unix"}
}
